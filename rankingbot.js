/**
 * Author: Pablo Maciá Linares (PML)
 * Date: 01/08/2019
 * SlackBot API: https://github.com/mishk0/slack-bot-api
 */

const SlackBot = require('slackbots');
const bot = new SlackBot({
	token: 'xoxb-965626488384-976123532018-j6MCc8j0LYkhADoEcxrwKrrC',
	name: 'rankingbot'
});
let botId = null;

const mongoose = require('mongoose');
const uri = 'mongodb://localhost/rankingBot';
const db = mongoose.createConnection(uri, { useNewUrlParser: true });
const Schema = mongoose.Schema;
const userSchema = new Schema({
	id: String,
	name: String,
	realName: String,
	image: String,
	points: Number,
	monthlyPoints: Number,
	dailyPoints: Number
});
const User = db.model('user', userSchema);
const pointsConfigSchema = new Schema({
	monthlyRankingResetDate: Date,
	dailyPointsResetDate: Date
});
const PointsConfig = db.model('pointsconfig', pointsConfigSchema);
let pointsConfig = null;
const maxDailyPoints = 5;
const daemonsInterval = 10 * 60 * 1000; //10 minutes

let botCommands = [
	{ desc: 'Give X points to another user', command: '@user +Xp <reason>' },
	{ desc: 'Take X points from another user', command: '@user -Xp <reason>' },
	{ desc: 'Display top 5 Global Ranking', command: '<@{botId}> top' },
	{ desc: 'Display top X Global Ranking', command: '<@{botId}> top X' },
	{ desc: 'Display top 5 Monthly Ranking', command: '<@{botId}> mtop' },
	{ desc: 'Display top X Monthly Ranking', command: '<@{botId}> mtop X' },
	{ desc: 'Display tail 5 Global Ranking', command: '<@{botId}> tail' },
	{ desc: 'Display tail X Global Ranking', command: '<@{botId}> tail X' },
	{ desc: 'Display tail 5 Monthly Ranking', command: '<@{botId}> mtail' },
	{ desc: 'Display tail X Monthly Ranking', command: '<@{botId}> mtail X' },
	{ desc: 'Display user points and position in the Global Ranking', command: '<@{botId}> @user' },
	{ desc: 'Display user points and position in the Monthly Ranking', command: '<@{botId}> @user monthly' },
	{ desc: 'Display your points', command: '<@{botId}> points' },
	{ desc: 'Display this list', command: '<@{botId}> help' },
];

bot.on('start', () => {
	setupUsersCollection();
	setupUserPoints();
});
bot.on('error', (err) => console.error(err));
bot.on('message', (data) => {
	if (data.type !== 'message') {
		return;
	}

	let message = data.subtype === 'message_changed' ? data.message.text : data.text;
	let msgUser = data.subtype === 'message_changed' ? data.message.user : data.user;
	if (message && msgUser) {
		handleMessage(
			data.channel,
			data.subtype === 'message_changed' ? data.message.text : data.text,
			data.subtype === 'message_changed' ? data.message.user : data.user
		);
	}
});

function setupUsersCollection() {
	bot.getUsers().then(
		result => {
			let slackUsers = result.members;
			slackUsers.forEach(slackUser => {
				if (slackUser.name == 'rankingbot') {
					botId = slackUser.id;
					console.log('Bot Id: ' + botId);
				}
				User.findOne({ id: slackUser.id }).exec((err, user) => {
					if (err) {
						console.error(err);
					} else {
						if (user === null && !slackUser.deleted) {
							User.create(
								{
									id: slackUser.id,
									name: slackUser.name,
									realName: slackUser.profile.real_name,
									image: slackUser.profile.image_48,
									points: 0,
									monthlyPoints: 0,
									dailyPoints: maxDailyPoints
								}
							);
						} else if (slackUser.deleted) {
							User.deleteOne({ id: slackUser.id }, (err) => {
								if (err) {
									console.error(err);
								}
							});
						} else if (user.name != slackUser.name || user.realName != slackUser.profile.real_name || user.image != slackUser.profile.image_48) {
							User.updateOne(
								{ id: slackUser.id },
								{
									name: slackUser.name,
									realName: slackUser.profile.real_name,
									image: slackUser.profile.image_48
								}
							);
						}
					}
				});
			});
		},
		error => {
			console.error(error);
		}
	);
}

function setupUserPoints() {
	PointsConfig.findOne({}).exec((err, config) => {
		if (err) {
			console.error(err);
		} else {
			if (config === null) {
				PointsConfig.create({
					monthlyRankingResetDate: new Date(0, 0, 0, 0, 0, 0, 0),
					dailyPointsResetDate: new Date(0, 0, 0, 0, 0, 0, 0)
				}).then(config => {
					pointsConfig = config;
					initPointsDaemons();
				});
			} else {
				pointsConfig = config;
				initPointsDaemons();
			}
		}
	});
}

function initPointsDaemons() {
	checkMonthlyRankingReset();
	checkDailyPointsReset();

	setInterval(() => {
		checkMonthlyRankingReset();
		checkDailyPointsReset();
	}, daemonsInterval);
}

function checkMonthlyRankingReset() {
	const now = new Date();

	if (now.getTime() >= pointsConfig.monthlyRankingResetDate.getTime()) {
		displayMonthlyRankingResetMessage();
		setTimeout(() => {
			resetMonthlyRanking();
		}, 10000);
	}
}

function checkDailyPointsReset() {
	const now = new Date();

	if (now.getTime() >= pointsConfig.dailyPointsResetDate.getTime()) {
		resetDailyPoints();
	}
}

function displayMonthlyRankingResetMessage() {
	bot.getChannels()._value.channels.forEach(channel => {
		if (channel.is_member == true) {
			let message = '*THESE ARE THE BEST AND WORSE POSITIONS ON THE MONTHLY RANKING*\r\n';
			message += 'Congratulations to the winners and shame to the losers';
			bot.postMessage(channel.name_normalized, message);
			setTimeout(() => {
				getRankingTop(channel.name_normalized, [], true);
				getRankingTail(channel.name_normalized, [], true);
			}, 1000);
		}
	});
}

function resetMonthlyRanking() {
	let monthlyRankingResetDate = new Date();
	monthlyRankingResetDate.setDate(1);
	monthlyRankingResetDate.setMonth(monthlyRankingResetDate.getMonth() + 1);
	monthlyRankingResetDate.setHours(0, 0, 0);

	PointsConfig.updateMany({}, { monthlyRankingResetDate: monthlyRankingResetDate }, (err) => {
		if (err) {
			console.error(err);
		} else {
			PointsConfig.findOne({}).exec((err, config) => {
				if (err) {
					console.error(err);
				} else {
					pointsConfig = config;
					User.updateMany({}, { monthlyPoints: 0 }, (err) => {
						if (err) {
							console.error(err);
						} else {
							console.log('Monthly ranking reseted');
						}
					});
				}
			});
		}
	});
}

function resetDailyPoints() {
	let dailyPointsResetDate = new Date();
	dailyPointsResetDate.setDate(dailyPointsResetDate.getDate() + 1);
	dailyPointsResetDate.setHours(0, 0, 0);

	PointsConfig.updateMany({}, { dailyPointsResetDate: dailyPointsResetDate }, (err) => {
		if (err) {
			console.error(err);
		} else {
			PointsConfig.findOne({}).exec((err, config) => {
				if (err) {
					console.error(err);
				} else {
					pointsConfig = config;
					User.updateMany({}, { dailyPoints: maxDailyPoints }, (err) => {
						if (err) {
							console.error(err);
						} else {
							console.log('Daily points reseted');
							performDailyLottery();
						}
					});
				}
			});
		}
	});
}

function performDailyLottery() {
	User.find({}).exec((err, users) => {
		if (err) {
			console.error(err);
		} else {
			let winner = users[Math.floor(Math.random() * users.length)];
			User.updateOne(
				{id: winner.id},
				{points: (winner.points + 5), monthlyPoints: (winner.monthlyPoints + 5), dailyPoints: (winner.dailyPoints + 5)},
				(err, res) => {
					if (err) {
						console.error(err);
					} else {
						let message = ':coin: *THE WINNER OF THE DAILY LOTTERY IS';
						message += ' <@' + winner.id + '>* :coin:\r\n';
						message += 'You won 5 free points, and 5 extra points to share, Congratulations!!\r\n';
						bot.getChannels()._value.channels.forEach(channel => {
							if (channel.is_member == true) {
								bot.postMessage(channel, message);
							}
						});
					}
				}
			);
		}
	});
}

function handleMessage(channel, message, msgUser) {
	if (/^<@.+>\s+[\+\-]\d+p/i.test(message)) {
		changeUserPoints(channel, message, msgUser);
	} else if (message.startsWith('<@' + botId + '>')) {
		let command = message.split(/\s+/gi);

		if (/<@.+>/gi.test(command[1])) {
			getUserRanking(channel, command);
		} else {
			switch (command[1]) {
				case 'help':
					displayBotCommands(channel);
					break;
				case 'points':
					getUserPoints(channel, msgUser);
					break;
				case 'top':
					getRankingTop(channel, command);
					break;
				case 'tail':
					getRankingTail(channel, command);
					break;
				case 'mtop':
					getRankingTop(channel, command, true);
					break;
				case 'mtail':
					getRankingTail(channel, command, true);
					break;
				default:
					console.error('Unknown command received');
					console.error(command);
					break;
			}
		}
	}
}

function changeUserPoints(channel, message, msgUserId) {
	let userMatch = message.match(/<@.+>/i)[0];
	let userId = userMatch.substring(2, userMatch.length - 1);
	let pointsMatch = message.match(/[\+\-]\d+p/i)[0];
	let points = parseInt(pointsMatch.substring(0, pointsMatch.length - 1));
	let reason = message.substring(message.indexOf(pointsMatch) + pointsMatch.length);

	Promise.all([
		User.findOne({ id: msgUserId }),
		User.findOne({ id: userId })
	]).then(results => {
		const [msgUser, user] = results;

		if (msgUserId !== userId) {
			if (msgUser.dailyPoints >= Math.abs(points)) {
				User.updateOne({ id: userId }, { points: (user.points + points), monthlyPoints: (user.monthlyPoints + points) }, (err, res) => {
					if (err) {
						console.error(err);
					} else {
						//onChangePointsMessage(channel, userId, msgUserId, points, user.points + points, reason);
						onChangePointsShortMessage(channel, points);
						User.updateOne({ id: msgUserId }, { dailyPoints: (msgUser.dailyPoints - Math.abs(points)) }, (err, res) => {
							if (err) {
								console.error(err);
							}
						});
					}
				});
			} else {
				bot.postMessage(
					channel,
					'Sorry, you don´t have enough points <@' + msgUserId + '>'
				);
			}
		} else {
			bot.postMessage(
				channel,
				'You can´t give or take points off yourself <@' + msgUserId + '> :troll:'
			);
		}
	}).catch(err => console.error(err));
}

function onChangePointsMessage(channel, userId, msgUser, points, totalPoints, reason) {
	let pointsLabel = Math.abs(points) === 1 ? 'Point' : 'Points';
	let totalPointsLabel = Math.abs(totalPoints) === 1 ? 'Point' : 'Points';
	let message = points > 0
		? 'Congrats <@' + userId + '>, <@' + msgUser + '> has given you *' + Math.abs(points) + '* ' + pointsLabel + '! :coin:'
		: 'Sorry <@' + userId + '>, <@' + msgUser + '> has taken *' + Math.abs(points) + '* ' + pointsLabel + ' from you! :cry:';
	let pointsReason = reason.length !== 0 ? ' Reason: _' + reason + '_.' : '';

	bot.postMessage(
		channel,
		message + '\r\nNow you have *' + totalPoints + '* ' + totalPointsLabel + '.' + pointsReason
	);
}

function onChangePointsShortMessage(channel, points) {
	let message = points > 0 ? ':coin:' : ':poop:';
	bot.postMessage(
		channel,
		message
	);
}

function displayBotCommands(channel) {
	let message = 'Commands:\r\n';

	botCommands.forEach(command => {
		message += '\t•' + command.desc + ': ';
		message += '*' + command.command.replace(/{botId}/gi, botId) + '*\r\n';
	});

	bot.postMessage(channel, message);
}

function getUserRanking(channel, command) {
	let userMatch = command[1].match(/<@.+>/i)[0];
	let userId = userMatch.substring(2, userMatch.length - 1);
	let monthly = command.length >= 3 && (command[2] === 'monthly' || command[2] === 'm');
	let orderBy = monthly ? { monthlyPoints: -1 } : { points: -1 };

	User.find(
		{},
		['id', 'name', 'realName', 'image', 'points', 'monthlyPoints'],
		{ sort: orderBy },
		(err, users) => {
			if (err) {
				console.error(err);
			} else {
				for (let i = 0; i < users.length; i++) {
					let user = users[i];
					if (userId === user.id) {
						bot.postMessage(
							channel,
							'<@' + user.id + '> has ' + (monthly ? user.monthlyPoints : user.points) + ' points. Position in the' + (monthly ? ' Monthly ' : ' ') + 'Ranking: ' + (i + 1) + '.'
						);
						break;
					}
				}
			}
		}
	);
}

function getUserPoints(channel, userId) {
	User.findOne({ id: userId }, (err, user) => {
		if (err) {
			console.error(err);
		} else {
			let message = '<@' + userId + '> points:\r\n';
			message += '\t• Global points: *' + user.points + '*\r\n';
			message += '\t• Monthly points: *' + user.monthlyPoints + '*\r\n';
			message += '\t• Daily points: *' + user.dailyPoints + '*\r\n';
			bot.postMessage(channel, message);
		}
	});
}

function getRankingTop(channel, command, monthly = false) {
	let rankingLimit = 5;
	if (command.length >= 3) {
		rankingLimit = /\d+/gi.test(command[2]) ? parseInt(command[2]) : rankingLimit;
	}
	createRanking(
		channel,
		':trophy: *Top of the' + (monthly ? ' Monthly ' : ' Global ') + 'Ranking* :trophy:',
		rankingLimit,
		monthly,
		true
	);
}

function getRankingTail(channel, command, monthly = false) {
	let rankingLimit = 5;
	if (command.length >= 3) {
		rankingLimit = /\d+/gi.test(command[2]) ? parseInt(command[2]) : rankingLimit;
	}
	createRanking(
		channel,
		':poop: *Tail of the' + (monthly ? ' Monthly ' : ' Global ') + 'Ranking* :poop:',
		rankingLimit,
		monthly,
		false
	);
}

function createRanking(channel, title, rankingLimit, monthly, desc) {
	let orderBy = monthly
		? { monthlyPoints: desc ? -1 : 1, name: desc ? 1 : -1 }
		: { points: desc ? -1 : 1, name: desc ? 1 : -1 };
	User.find(
		{},
		['id', 'name', 'realName', 'image', 'points', 'monthlyPoints'],
		{ skip: 0, limit: rankingLimit, sort: orderBy },
		(err, users) => {
			if (err) {
				console.error(err);
			} else {
				displayRanking(channel, title, users, monthly);
			}
		}
	);
}

function displayRanking(channel, title, users, monthly) {
	let message = title + '\r\n';
	message += 'Points\t\tName\r\n';

	users.forEach(user => {
		message += '*' + (monthly ? user.monthlyPoints : user.points) + '*\t\t' + user.realName + '\r\n';
	});

	bot.postMessage(channel, message);
}